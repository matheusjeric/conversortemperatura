#ifndef MEDIDOR_HPP
#define MEDIDOR_HPP
#include <string>

using namespace std;

class medidortemperatura {
private:
         
	float temperatura;
public:
     
	medidortemperatura();
        medidortemperatura(float temperatura);

	float getTemperatura();
	void setTemperatura (float temperatura);
        
        float converteC();
        float converteF();
        float converteK();
           

};

#endif      
