#ifndef KELVIN_HPP
#define KELVIN_HPP
#include "medidor_temperatura.hpp"

using namespace std;

class Kelvin: public  medidortemperatura {
public:
      
      Kelvin();
      Kelvin(float temperatura);
      float  converteC();
      float  converteF(); 
};
#endif
