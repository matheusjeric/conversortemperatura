#ifndef FAHRENHEIT_HPP
#define FAHRENHEIT_HPP
#include <string>
#include "medidor_temperatura.hpp"

using namespace std;

class Fahrenheit : public medidortemperatura{
public:
	Fahrenheit();
        Fahrenheit(float temperatura);	
	float converteK();
	float converteC();
};

#endif
