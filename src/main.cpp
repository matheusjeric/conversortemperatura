#include <string>
#include <iostream>
#include "kelvin.hpp"
#include "celsius.hpp"
#include "fahrenheit.hpp"

using namespace std;

int main(){
     
    float temperature,result;  
    int choose;
    cout << "Digite o valor da temperatura: "<< endl;
    cin >> temperature;
    cout << "Escolha o tipo da temperatura de entrada: \n 1-Celsius \n 2-Kelvin \n 3-Fahrenheit"<< endl;
    cin >> choose;
   
    if(choose == 1){ 
 
    Celsius * temp1 = new Celsius(temperature);
    result = temp1->converteK();
    cout << "Temperatura em Kelvin: " << result << endl;
    result = temp1->converteF();
    cout << "Temperatura em Fahrenheit: " << result << endl;
 

    }

	else if(choose == 2){ 
 
    Kelvin * temp1 = new Kelvin(temperature);
    result = temp1->converteC();
    cout << "Temperatura em celsius: " << result << endl;
    result = temp1->converteF();
    cout << "Temperatura em Fahrenheit: " << result << endl;
 

    }


	else if(choose == 3){ 
 
    Fahrenheit * temp1 = new Fahrenheit(temperature);
    result = temp1->converteK();
    cout << "Temperatura em Kelvin: " << result << endl;
    result = temp1->converteC();
    cout << "Temperatura em Celsius: " << result << endl;
 

    }


else{
    cout << "Escolha inválida" << endl; 
    }
return 0;
}
