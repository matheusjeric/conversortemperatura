#include <iostream>
#include "medidor_temperatura.hpp"

using namespace std;

medidortemperatura::medidortemperatura() {

	setTemperatura(0);
}
medidortemperatura::medidortemperatura(float temperatura) {

	setTemperatura(temperatura);
}

void medidortemperatura::setTemperatura(float temperatura){
	this->temperatura=temperatura;
}

float medidortemperatura::getTemperatura(){
	return temperatura;
}


