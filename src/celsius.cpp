#include <iostream>
#include "celsius.hpp"
#include <math.h>

using namespace std;

Celsius::Celsius(){

    setTemperatura(0);

}
Celsius::Celsius(float temperatura){

    setTemperatura(temperatura);

}

float Celsius::converteK(){ 
   
    return getTemperatura() + 273;
}

float Celsius::converteF(){ 
   
    return ((9*(getTemperatura()/5))-32);
}


